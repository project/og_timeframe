<?php

/**
 * @file
 * Configure date fields attached to groups as time frame for content.
 */

/**
 * Implements hook_date_field_instance_settings_form_alter().
 */
function og_timeframe_datefield_date_field_instance_settings_form_alter(&$form, $context) {
  $instance = $context['instance'];
  if (!og_is_group_type($instance['entity_type'], $instance['bundle'])) {
    return;
  }
  $og_content_bundles = og_get_all_group_content_bundle();
  if (empty($og_content_bundles['node'])) {
    return;
  }
  $type_options = array();
  foreach ($og_content_bundles['node'] as $bundle => $title) {
    if (og_is_group_content_type('node', $bundle)) {
      $type_options[$bundle] = $title;
    }
  }
  if ($context['field']['cardinality'] != 1) {
    drupal_set_message(t('Warning: Field is set to have multiple values. Only the first will be used.'), 'warning');
  }
  $impact_options = array(
    '' => t("Don't use"),
    'all' => t("All content associated with the group"),
  );
  if ($type_options) {
    $impact_options['bundles'] = ("Selected group content types");
  }
  $form['og_timeframe_datefield_impact'] = array(
    '#type' => 'radios',
    '#title' => t("Use as time frame for"),
    '#default_value' => isset($instance['settings']['og_timeframe_datefield_impact']) ? $instance['settings']['og_timeframe_datefield_impact'] : '',
    '#description' => t("Use this field as a time frame for posting content to the group. <em>Selected group content types</em> has higher precedence than <em>All content associated with the group</em>, so you can have two date fields attached where one serves as a default for all associated content and another that overrides the default for selected content types."),
    '#options' => $impact_options,
  );
  if ($type_options) {
    $form['og_timeframe_datefield_type'] = array(
      '#type' => 'select',
      '#title' => t("Group content types"),
      '#options' => $type_options,
      '#multiple' => TRUE,
      '#default_value' => isset($instance['settings']['og_timeframe_datefield_type']) ? $instance['settings']['og_timeframe_datefield_type'] : '',
      '#states' => array(
        'visible' => array('input[name="instance[settings][og_timeframe_datefield_impact]"]' => array('value' => 'bundles')),
      ),
    );
  }
}

/**
 * Implements hook_og_timeframe_handler_info().
 */
function og_timeframe_datefield_og_timeframe_handler_info() {
  $handlers = array();
  $group_types = og_get_all_group_bundle();
  // Find all date fields attached to group entity types and provide our handler
  // for the configured group content entity type.
  foreach ($group_types as $group_type => $group_bundles) {
    foreach ($group_bundles as $group_bundle => $label) {
      $instances = field_info_instances($group_type, $group_bundle);
      foreach ($instances as $field_name => $field) {
        if (empty($field['settings']['og_timeframe_datefield_impact'])) {
          continue;
        }
        // We are providing our handler, we just don't know what impact it has.
        $handler = array(
          '#class' => 'OgTimeframeGroupHandler',
          '#date_field_name' => $field_name,
        );
        if ($field['settings']['og_timeframe_datefield_impact'] == 'all') {
          // It's not a leaf handler, so if a more specific handler already has
          // been set for a bundle below this type, a single assign will
          // overwrite it. We have to use array_merge().
          $handlers[$group_type][$group_bundle]['node'] = array_merge($handlers[$group_type][$group_bundle]['node'], $handler);
        }
        elseif ($field['settings']['og_timeframe_datefield_impact'] == 'bundles') {
          // Per group content entity bundle handler.
          foreach ($field['settings']['og_timeframe_datefield_type'] as $content_bundle) {
            $handlers[$group_type][$group_bundle]['node'][$content_bundle] = $handler;
          }
        }
      }
    }
  }
  return $handlers;
}
